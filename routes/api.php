<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/createOwner', 'App\Http\Controllers\MainController@createOwner');
Route::post('/createDriver', 'App\Http\Controllers\MainController@createDriver');
Route::post('/createVehicle', 'App\Http\Controllers\MainController@createVehicle');
Route::get('/getReport', 'App\Http\Controllers\MainController@getReport');
Route::get('/getOwners', 'App\Http\Controllers\MainController@getOwners');
Route::get('/getDrivers', 'App\Http\Controllers\MainController@getDrivers');
Route::get('/exportReport', 'App\Http\Controllers\MainController@exportReport');
