CREATE TABLE IF NOT EXISTS `acme`.`owners` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `identification` VARCHAR(15) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `lastName` VARCHAR(100) NOT NULL,
  `address` VARCHAR(100) NOT NULL,
  `phone` VARCHAR(20) NOT NULL,
  `city` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `acme`.`drivers` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `identification` VARCHAR(15) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `lastName` VARCHAR(100) NOT NULL,
  `address` VARCHAR(100) NOT NULL,
  `phone` VARCHAR(20) NOT NULL,
  `city` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `acme`.`vehicles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `plate` VARCHAR(10) NOT NULL,
  `color` VARCHAR(45) NOT NULL,
  `brand` VARCHAR(100) NOT NULL,
  `type` ENUM("particular", "publico") NOT NULL,
  `fk_driver_id` INT(11) NOT NULL,
  `fk_owner_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_vehicles_drivers_idx` (`fk_driver_id` ASC),
  INDEX `fk_vehicles_owners1_idx` (`fk_owner_id` ASC),
  CONSTRAINT `fk_vehicles_drivers`
    FOREIGN KEY (`fk_driver_id`)
    REFERENCES `acme`.`drivers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_vehicles_owners1`
    FOREIGN KEY (`fk_owner_id`)
    REFERENCES `acme`.`owners` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;