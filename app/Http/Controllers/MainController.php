<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Owner;
use App\Models\Driver;
use App\Models\Vehicle;

class MainController extends Controller
{
    public function createOwner(Request $request)
    {
        $owner = new Owner;
        $owner->fill($request->all());
        $owner->save();
        return response()->json(['success' => true,
            'message' => 'New row created successfully', 'owner' => $owner]);
    }
    public function createDriver(Request $request)
    {
        $driver = new Driver;
        $driver->fill($request->all());
        $driver->save();
        return response()->json(['success' => true,
            'message' => 'New row created successfully', 'driver' => $driver]);
    }
    public function createVehicle(Request $request)
    {
        $vehicle = new Vehicle;
        $vehicle->fill($request->all());
        $vehicle->save();
        return response()->json(['success' => true,
            'message' => 'New row created successfully', 'vehicle' => $vehicle]);
    }
    public function getReport()
    {
        $report = DB::table('vehicles')
            ->join('owners', 'owners.id', '=', 'vehicles.fk_owner_id')
            ->join('drivers', 'drivers.id', '=', 'vehicles.fk_driver_id')
            ->select('vehicles.plate', 'vehicles.brand', 'owners.name as ownerName', 'owners.lastName as ownerLastName', 'drivers.name as driverName', 'drivers.lastName as driverLastName')
            ->get();
        if (!$report) {
            return response()->json(['success' => false, 'message' => "There isn't records yet"]);
        }
        return response()->json(['success' => true, 'report' => $report]);
    }
    public function getOwners()
    {
        $owners = DB::table('owners')
            ->select('id', 'name', 'lastName')
            ->get();

        if (!$owners) {
            return response()->json(['success' => false, 'message' => "There isn't records yet"]);
        }
        return response()->json(['success' => true, 'owners' => $owners]);
    }
    public function getDrivers()
    {
        $drivers = DB::table('drivers')
            ->select('id', 'name', 'lastName')
            ->get();

        if (!$drivers) {
            return response()->json(['success' => false, 'message' => "There isn't records yet"]);
        }
        return response()->json(['success' => true, 'drivers' => $drivers]);
    }
}
